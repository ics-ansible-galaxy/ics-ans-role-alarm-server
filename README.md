# ics-ans-role-alarm-server

Ansible role to install Alarm server.

## Role Variables

```yaml
alarm_server_version: 4.6.0-SNAPSHOT
alarm_server_install_dir: /opt
alarm_server_config_dir: /opt/alarm-server-config
alarm_server_config_urls: []
alarm_server_settings: {}
alarm_server_settings_dir: /opt/alarm-server-settings
alarm_server_config_force: false
alarm_server_logging_level: INFO
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-alarm-server
```

## License

BSD 2-clause
