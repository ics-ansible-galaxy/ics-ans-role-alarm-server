import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_service(host):
    for config in ['TEST', 'TEST2']:
        assert host.service("alarm-server@{}".format(config)).is_running


def test_configs(host):
    for config in ['TEST', 'TEST2']:
        with host.sudo(user="alarm-server"):
            export_file = "/tmp/TEST-export.xml"
            expected_content = """<?xml version="1.0" encoding="UTF-8"?>
<config name="{}">
  <component name="TEST Alarms">
    <pv name="Test-TEST:TEST-AA-12345:AA">
      <description>Test alarm</description>
      <enabled>true</enabled>
      <latching>true</latching>
      <annunciating>true</annunciating>
      <guidance>
        <title>Test</title>
        <details>Test</details>
      </guidance>
      <display>
        <title>Test</title>
        <details>Test</details>
      </display>
    </pv>
  </component>
</config>
""".format(config)
            host.run("rm -f " + export_file)
            host.run("java -jar /opt/service-alarm-server-5.0.019/service-alarm-server-5.0.019.jar -config {} -export {}".format(config, export_file))
            assert host.file(export_file).content_string == expected_content
